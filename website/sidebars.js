module.exports = {
  docs: [
    'plano-leitura',
    {
      type: 'category',
      label: 'Modelo de Memória',
      items: [
        'memorymodel/memory-model-c',
        'memorymodel/memory-model-krebbers',
        'memorymodel/memory-model-safety',
      ],
    },

  ],
};

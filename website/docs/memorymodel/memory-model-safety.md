---
title: Memory Safety
---

A mini-ML language that allows reasoning and capturing properties over the memory safety of a program. Memory access errors are explicitly defined in the language.

---

## Memory Model

We assume that memory is conceptually infinitely large, and that memory addresses are never reused. As such, memory that is freed is never reallocated and stays permanently undefined.
This also permits indentifying exactly the memory safety violations.

### Definition

We represent the memory as a function from pointers to memoption, where:

$memoption\ 'a\colon \\ 
	\quad |\ Null \quad \quad \text{(-- Not\ initialized --)}\\
	\quad |\ Freed \quad \ \  \text{(-- Previously initialized and was de-allocated/freed --)}\\ 
	\quad |\ Init\ 'a \quad \  \text{(-- Initialized --)}$

$mem\ 'a\colon \\ \quad loc\ 'a\to memoption$

The empty memory is defines as

$empty\_memory()\colon \\
	\quad \forall x, \ x \in loc. \ \  x \to Null$

### Functions

We define the following functions that we expose to the programmer.

- $fresh\_loc\colon \ mem\  'a \to loc\ 'a$

 Given a memory, returns a fresh location.

- $update\_mem\colon \ mem\ 'a \ \times \ loc \ 'a \ \times\ 'a \to mem\ 'a$

  Update a specific location with the given value. Returns the updated memory.

- $free\_ref\colon \ mem \ 'a \ \times \ loc \ 'a \to mem \ 'a$

  Frees a specific location. Returns the updated memory.

- $get\_ref\colon \ mem\ 'a \ \times \ loc \ 'a\  \to \ 'a$

  Given a memory and a location, returns the value associated to the location.

---
## Mini-ML Language

We now define a mini-ML language that uses the memory model above. 

### Syntax

<!-- The syntax of the language is the following. -->

$v \quad \Coloneqq \quad () \ \ | \ \ b \ \ | \ \ n \ \ |\ \   loc\ v$

$op \quad \Coloneqq \quad + \ \ | \ \ - \ \ | \ \ \leq \ \ |\ \ \lt \ \ |\ \ ==$

$e \quad \Coloneqq 
	\\ \quad | \ \ x \ \ 
	\\\quad | \ \ v \ \ 
	\\\quad | \ \ \text{let} \ x \coloneqq e_1 \text{ in } e_2 \ \ 
	\\\quad |\ \ e_1 ; e_2 \ \ 
	\\\quad |\ \ e_1\ \ op\ \ e_2
	\\\quad |\ \ \text{if } e_1 \text{ then } e_2 \text{ else } e_3
	\\\quad |\ \ \text{while } e_1 \ \{ e_2 \}
	\\\quad |\ \ \text{alloc } e
	\\\quad |\ \ \text{free } e
	\\\quad |\ \ !e
	\\\quad |\ \ e_1 \coloneqq e_2$

We then define both natural and structural operational semantics.

### Natural semantics



The WhyML code is:

```ocaml
inductive big_step (expr) (mem value) (semantic_result) (mem value) =
  | Val_big_step: forall m v. big_step (EVal v) m (Value v) m
  | Let_big_step: forall y e1 e2 m1 v1 m2.
     big_step e1 m1 (Value v1) m2 ->
     forall v2 m3. big_step (subst y v1 e2) m2 v2 m3 ->
     big_step (ELet y e1 e2) m1 v2 m3
  | Seq_big_step: forall e1 m1 v1 m2 e2 v2 m3 .
     big_step e1 m1 (Value v1) m2 ->
     big_step e2 m2 v2 m3 ->
     big_step (ESeq e1 e2) m1 v2 m3
  | If_big_step_true: forall m1 m2 m3 e1 e2 e3 v.
     big_step e1 m1 (Value (VBool true)) m2 ->
     big_step e2 m2 v m3 ->
     big_step (EIf e1 e2 e3) m1 v m3
  | If_big_step_false: forall m1 m2 m3 e1 e2 e3 v.
     big_step e1 m1 (Value (VBool false)) m2 ->
     big_step e3 m2 v m3 ->
     big_step (EIf e1 e2 e3) m1 v m3
  | While_big_step_true: forall m1 m2 m3 m4 e1 e2 v1 v2.
     big_step e1 m1 (Value (VBool true)) m2 ->
     big_step e2 m2 (Value v1) m3 ->
     big_step (EWhile e1 e2) m3 v2 m4 ->
     big_step (EWhile e1 e2) m1 v2 m4
  | While_big_step_false: forall m1 m2 e1 e2.
     big_step e1 m1 (Value (VBool false)) m2 ->
     big_step (EWhile e1 e2) m1 (Value VUnit) m2
  | Op_big_step: forall op e1 e2 m1 v1 m2.
     big_step e1 m1 (Value v1) m2 ->
     forall v2 m3. big_step e2 m2 (Value v2) m3 ->
     forall v. eval_bin_op op v1 v2 = Some v ->
     big_step (EOp op e1 e2) m1 (Value v) m3
  | Alloc_big_step: forall m1 m2 e v.
     big_step e m1 (Value v) m2 ->
     let l = fresh_loc m2 in
     big_step (EAlloc e) m1 (Value (VLoc l)) (update_mem m2 l v)
  | Free_big_step: forall m1 m2 e l.
     big_step e m1 (Value (VLoc l)) m2 ->
     is_allocated m2 l -> 
     big_step (EFree e) m1 (Value VUnit) (safe_free_ref m2 l)
  | Load_big_step: forall e m1 v m2 l.
     big_step e m1 (Value (VLoc l)) m2 ->
     is_allocated m2 l ->
     safe_get_ref m2 l = v ->
     big_step (ELoad e) m1 (Value v) m2
  | Store_big_step: forall m1 m2 m3 e1 e2 l v.
     big_step e1 m1 (Value (VLoc l)) m2 ->
     big_step e2 m2 (Value v) m3 ->
     is_allocated m3 l ->
     big_step (EStore e1 e2) m1 (Value VUnit) (update_mem m3 l v)
```

### Structural semantics

We defined the structural semantics with evaluation contexts. The head reductions are defined as follows:
```ocaml
inductive head_step (expr) (mem value) (semantic_expr) (mem value) =
  | Let_headstep: forall m y e2 v1.
     head_step (ELet y (EVal v1) e2) m (Expr (subst y v1 e2)) m
  | Seq_headstep: forall m e2 v1.
     head_step (ESeq (EVal v1) e2) m (Expr e2) m
  | If_headstep_true: forall m e2 e3.
     head_step (EIf (EVal (VBool true)) e2 e3) m (Expr e2) m
  | If_headstep_false: forall m e2 e3.
     head_step (EIf (EVal (VBool false)) e2 e3) m (Expr e3) m
  | While_headstep: forall m e1 e2.
     head_step (EWhile e1 e2) m (Expr (EIf e1 (ESeq e2 (EWhile e1 e2)) (EVal VUnit))) m
  | Op_headstep: forall m op v1 v2 v.
     eval_bin_op op v1 v2 = Some v ->
     head_step (EOp op (EVal v1) (EVal v2)) m (Expr (EVal v)) m
  | Alloc_headstep: forall m v.
     let l = fresh_loc m in
     head_step (EAlloc (EVal v)) m (Expr (EVal (VLoc l))) (update_mem m l v)
  | Free_headstep: forall m l.
     is_allocated m l ->
     head_step (EFree (EVal (VLoc l))) m (Expr (EVal VUnit)) (safe_free_ref m l)
  | Load_headstep: forall m l v.
     is_allocated m l ->
     safe_get_ref m l = v ->
     head_step (ELoad (EVal (VLoc l))) m (Expr (EVal v)) m
  | Store_headstep: forall m l v.
     is_allocated m l ->
     head_step (EStore (EVal (VLoc l)) (EVal v)) m (Expr (EVal VUnit)) (update_mem m l v)  
```

The next step is to lift head reduction to whole terms, by allowing redexes to be reduced at any evaluation position, instead of just at the head position.
We define this lifting by means of _evaluation contexts_. 

$E \coloneqq \\ \quad | \ \ \square \ \ 
	\\ \quad | \ \ \text{let } x \coloneqq E \text{ in } e_2 \ \ 
	\\ \quad | \ \ E \ op \ e_2 \ \ 
	\\ \quad | \ \ e_1 \ op \ E \ \ 
	\\ \quad | \ \ \text{if } E \text{ then } e_2 \text{ else } e_3
	\\ \quad | \ \ \text{alloc } E
	\\ \quad | \ \ \text{free } E
	\\ \quad | \ \ !E
	\\ \quad | \ \ E \coloneqq e_2
	\\ \quad | \ \ e_1 \coloneqq E$

We formalize evaluation contexts in two stages. First we define
_evaluation context items_:

```ocaml
type ctx_item =
    | LetCtx string expr
    | SeqCtx expr
    | OpCtxL bin_op expr
    | OpCtxR bin_op value
    | IfCtx expr expr
    | AllocCtx 
    | FreeCtx 
    | LoadCtx 
    | StoreCtxL expr 
    | StoreCtxR value
```
And then define evaluation contexts as lists of evaluation contexts.
```ocaml 
type ctx = list ctx_item
```

Furthermore, we define a function for putting an expression in the hole of an evaluation context item, and then lift said function to evaluation contexts.

```ocaml
function fill_item (ctxi: ctx_item) (e : expr) : expr 
= match ctxi with
	| LetCtx s e2 -> ELet s e e2
	| SeqCtx e2 -> ESeq e e2
	| OpCtxL op e2 -> EOp op e e2
	| OpCtxR op v1 -> EOp op (EVal v1) e
	| IfCtx e2 e3 -> EIf e e2 e3
	| AllocCtx -> EAlloc e
	| FreeCtx -> EFree e
	| LoadCtx -> ELoad e
	| StoreCtxL e2 -> EStore e e2
	| StoreCtxR v1 -> EStore (EVal v1) e  
end
    
function fill (ctx : ctx) (e : expr) : expr 
= match ctx with
	| Nil -> e
	| Cons x l -> fill_item x (fill l e)
end
  
inductive step expr (mem value) semantic_expr (mem value) =
	| do_step: forall ctx m1 m2 e1 e2.
     head_step e1 m1 (Expr e2) m2 ->
     step (fill ctx e1) m1 (Expr (fill ctx e2)) m2
```




### Extending the semantics to accommodate errors

Insted of the semantics producing only values, we extend them to also produce errors:

```
type semantic_result =
    | Value value
    | VError error
```

There are 4 types of errors, which correspond to the so called memory access errors:

- use of uninitialized memory
- use after free
- illegal free (of an already-freed pointer, or a non-malloced pointer)
- null pointer dereference

Each one of these errors corresponds to a construct in our _error_ type:

```
type error = 
    | Use_of_Null
    | Use_After_Free
    | Double_Free
    | Free_None
```

Now, we define the rules that produce errors:

```
inductive big_step (expr) (mem value) (semantic_result) (mem value) =
  ...  
  | Free_error_free_none_big_step: forall m1 m2 e l.
     big_step e m1 (Value (VLoc l)) m2 ->
     m2.refs l = Null ->
     big_step (EFree e) m1 (VError Free_None) m2
  | Free_error_double_free_big_step: forall m1 m2 e l.
     big_step e m1 (Value (VLoc l)) m2 ->
     m2.refs l = Freed ->
     big_step (EFree e) m1 (VError Double_Free) m2
  | Load_use_after_free_big_step: forall e m1 m2 l.
     big_step e m1 (Value (VLoc l)) m2 ->
     m2.refs l = Freed ->
     big_step (ELoad e) m1 (VError Use_After_Free) m2
  | Load_null_deref_big_step: forall e m1 m2 l.
     big_step e m1 (Value (VLoc l)) m2 ->
     m2.refs l = Null ->
     big_step (ELoad e) m1 (VError Use_of_Null) m2
  | Store_after_free_error_big_step: forall m1 m2 m3 e1 e2 l v.
      big_step e1 m1 (Value (VLoc l)) m2 ->
      big_step e2 m2 (Value v) m3 ->
      m3.refs l = Freed ->
      big_step (EStore e1 e2) m1 (VError Use_After_Free) m3
  | Store_use_of_null_error_big_step: forall m1 m2 m3 e1 e2 l v.
      big_step e1 m1 (Value (VLoc l)) m2 ->
      big_step e2 m2 (Value v) m3 ->
      m3.refs l = Null ->
      big_step (EStore e1 e2) m1 (VError Use_of_Null) m3
```

Moreover, we need to propagate the errors through every other rule we previously defined.

```
inductive big_step (expr) (mem value) (semantic_result) (mem value) =
  ...  
  | Let_error_big_step: forall y e1 e2 m1 m2 v.
     big_step e1 m1 (VError v) m2 ->
     big_step (ELet y e1 e2) m1 (VError v) m2
  | Let_error_2_big_step: forall y e1 e2 m1 v1 m2 v.
     big_step e1 m1 (Value v1) m2 ->
     forall m3. big_step (subst y v1 e2) m2 (VError v) m3 ->
     big_step (ELet y e1 e2) m1 (VError v) m3
  | Seq_error_big_step: forall e1 m1 m2 e2 v.
     big_step e1 m1 (VError v) m2 ->
     big_step (ESeq e1 e2) m1 (VError v) m2
  | If_error_big_step: forall m1 m2 e1 e2 e3 v.
     big_step e1 m1 (VError v) m2 ->
     big_step (EIf e1 e2 e3) m1 (VError v) m2
  | While_error_big_step_true: forall m1 m2 m3 e1 e2 v.
     big_step e1 m1 (Value (VBool true)) m2 ->
     big_step e2 m2 (VError v) m3 ->
     big_step (EWhile e1 e2) m1 (VError v) m3
  | While_error_big_step: forall m1 m2 e1 e2 v.
     big_step e1 m1 (VError v) m2 ->
     big_step (EWhile e1 e2) m1 (VError v) m2
  | Op_error_big_step: forall op e1 e2 m1 m2 v.
     big_step e1 m1 (VError v) m2 ->
     big_step (EOp op e1 e2) m1 (VError v) m2
  | Op_error_2_big_step: forall op e1 e2 m1 v1 m2 v.
     big_step e1 m1 (Value v1) m2 ->
     forall m3. big_step e2 m2 (VError v) m3 ->
     big_step (EOp op e1 e2) m1 (VError v) m3
  | Alloc_error_big_step: forall m1 m2 e v.
     big_step e m1 (VError v) m2 ->
     big_step (EAlloc e) m1 (VError v) m2
  | Free_error_big_step: forall m1 m2 e v. 
     big_step e m1 (VError v) m2 ->
     big_step (EFree e) m1 (VError v) m2
  | Load_error_big_step: forall e m1 m2 v.
     big_step e m1 (VError v) m2 ->
     big_step (ELoad e) m1 (VError v) m2
  | Store_error_big_step: forall m1 m2 e1 e2 v.
     big_step e1 m1 (VError v) m2 ->
     big_step (EStore e1 e2) m1 (VError v) m2
  | Store_error_2_big_step: forall m1 m2 m3 e1 e2 l v.
     big_step e1 m1 (Value (VLoc l)) m2 ->
     big_step e2 m2 (VError v) m3 ->
     big_step (EStore e1 e2) m1 (VError v) m3
```

### Results proved

#### Natural semantics

|     Result      	|
|:------------------|
|    determinism    |
|    coincidence with structural semantics    |

#### Structural semantics

|     Result      	|
|:------------------|
|    head_step determinism    |
|    step determinism    |
|    steps determinism    |
|    steps transitive    |
|    coincidence with natural semantics    |


---

## Reasoning over simple programs

### Definition of memory safety

```ocaml
predicate memory_safe (e: expr) (m: mem value)
  = (exists m1 v. big_step e m (Value v) m1)
```

### Proofs

We show herein, specific proofs about a certain program:

|           	|      Proofs     	|
|:------------------:|:------------------|
| 1 |    Free of a non-allocated pointer is not safe    | 
| 2 |    Free of an allocated pointer is safe    | 
| 3 |    Load of a non-allocated pointer is not safe    | 
| 4 |    Load of an allocated pointer is safe    | 
| 5 |    Load after free is not safe    |
| 6 |    Double free is not safe    |


1. Free of a non-allocated pointer is not safe
```
lemma free_none_not_safe: forall l m.
    not is_allocated m l ->
    not memory_safe  (EFree (EVal (VLoc l))) m
```

2. Free of an allocated pointer is safe
```
lemma free_is_safe: forall l m.
    is_allocated m l ->
    memory_safe  (EFree (EVal (VLoc l))) m
```

3. Load of a non-allocated pointer is not safe

```
lemma load_none_not_safe: forall l m.
    not is_allocated m l ->
    not memory_safe  (ELoad (EVal (VLoc l))) m
```

4. Load of an allocated pointer is safe
```
lemma load_is_safe: forall l m.
    is_allocated m l ->
    memory_safe  (ELoad (EVal (VLoc l))) m
```

5. Load after free is not safe
```
lemma not_safe_load_after_free:
    not memory_safe (ELet "x" (EAlloc (EVal (VNat 1)))
      (ESeq (EFree (EVar "x")) (ELoad (EVar "x")))) (empty_memory ())
```

6. Double free is not safe
```
lemma example_double_free:
    not memory_safe (ELet "x" (EAlloc (EVal (VNat 1)))
      (ESeq (EFree (EVar "x")) (EFree (EVar "x")))) (empty_memory ())
```




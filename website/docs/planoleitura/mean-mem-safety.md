---
title: The Meaning of Memory Safety
---

3 contributions:
- formalize local reasoning principles supported by an ideal notion of memory safety.
- evaluate pragmatically motivated relaxations of the ideal notion of memory safety.
- showing that their characterization applies to more realistic settings.

---

### An Idealized Memory-Safe Language



---
title: Multicore Semantics - Making Sense of Relaxed Memory
---

Congratulations on making it this far!

You have learned the **basics of Docusaurus** and made some changes to the **initial template**.

But Docusaurus has **much more to offer**!


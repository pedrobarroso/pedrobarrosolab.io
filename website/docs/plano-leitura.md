---
title: Plano de Leitura
slug: /
---

|  Read  |  No.  |                                            Title                                                 |   Author(s)  |	Conference	  |   PDF  |
|--------|-------|:-------------------------------------------------------------------------------------------------|:------------:|:---------------:|:------:|
|    | 1  | [The Meaning of Memory Safety](/docs/planoleitura/mean-mem-safety/) | Amorim, Pierce and Hritcu | POST'18 																	 | [[link]](https://arxiv.org/pdf/1705.07354.pdf) |
|    | 2  | [Memory-Safety Challenge Considered Solved? An In-Depth Study with All Rust CVEs](/docs/planoleitura/mem-safety-challenge) | Xu, Chen, et al | ACM               | [[link]](https://dl.acm.org/doi/pdf/10.1145/3466642) |
|    | 3  | [Bounding Data Races in Space and Time](/docs/planoleitura/bounding-data-races) | Dolan, et al | PLDI'18                                                          | [[link]](https://kcsrk.info/papers/pldi18-memory.pdf) |
|    | 4  | [Retrofitting Parallelism onto OCaml](/docs/planoleitura/fitting-par-ocaml) | Dolan, et al | ICFP                                                              | [[link]](https://dl.acm.org/doi/pdf/10.1145/3408995) |
|    | 5  | [Multicore Semantics - Making Sense of Relaxed Memory](/docs/planoleitura/multicore-semantics) | Sewell, et al | Slides                                           | [[link]](https://www.cl.cam.ac.uk/~pes20/slides-acs-2020.pdf) |
|    | 6  | [Iris from the ground up - A modular foundation for higher-order concurrent separation logic](/docs/planoleitura/iris-ground-up) | Jung, Krebbers, et al | JFP         | [[link]](https://cs.au.dk/~birke/papers/iris-journal.pdf) |
|    | 7  | [Safe systems programming in Rust](/docs/planoleitura/safe-systems-rust) | Jung, Krebbers, et al | ACM                                                                 | [[link]](https://dl.acm.org/doi/pdf/10.1145/3418295) |
|    | 8  | [Strong logic for weak memory](/docs/planoleitura/strong-logic-weak-mem) | Kaiser, Dreyer, et al | ECOOP'17                                                                 | [[link]](https://plv.mpi-sws.org/igps/igps-full.pdf) |
|    | 9  | [The essence of higher-order concurrent separation logic](/docs/planoleitura/essence-of-ho-sl) | Jung, Krebbers, et al | ESOP'17                                           | [[link]](https://iris-project.org/pdfs/2017-esop-iris3-final.pdf) |
|    | 10 | [SteelCore - An Extensible Concurrent Separation Logic](/docs/planoleitura/steelcore) | Swamy, et al | ICFP'20                                                    | [[link]](https://www.fstar-lang.org/papers/steelcore/steelcore.pdf) |
|    | 11 | [Making weak memory models fair](/docs/planoleitura/weak-memory-fair) | Lahav, Vafeiadis, et al | OOPSLA'21                                                                    | [[link]](https://dl.acm.org/doi/pdf/10.1145/3485475) |
|    | 12 | [Verifying Systems C Code with Separation-Logic Refinement Types](/docs/planoleitura/verifying-systems-c) | Pulte, Sewell, et al | POPL'22                                | [[link]](https://www.cl.cam.ac.uk/~nk480/cn.pdf) |
|    | 13 | [Simuliris - A Separation Logic Framework for Verifying Concurrent Program Optimizations](/docs/planoleitura/simuliris) | Jung, Krebbers, et al | POPL'21                  | [[link]](https://iris-project.org/pdfs/2022-popl-simuliris.pdf) |
|    | 14 | [Interactive Proofs in Higher-order](/docs/planoleitura/inter-proofs-ho)  | Krebbers, et al | POPL'17                                                                | [[link]](https://iris-project.org/pdfs/2017-popl-proofmode-final.pdf) |
|    | 15 | [Kater - Automating Weak Memory Model Metatheory and Consistency Checking](/docs/planoleitura/kater-weak-memory) | Lahav, Vafeiadis, et al | POPL'22                         | [[link]](https://people.mpi-sws.org/~viktor/papers/popl2023-kater.pdf) |